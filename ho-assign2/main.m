//
//  main.m
//  ho-assign2
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-16.
//  Copyright (c) 2014 beta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConsoleLine.h"

// Project:      assign-2-1-2013
// File:         main.m
// Author:       Alice Wunderland
// Date:         Sep 14, 2013
// Course:       IMG204
//
// Problem Statement:
// Write an application that prompts for and reads integer values for speed
// and distance traveled, then prints the time required for the trip as a
// floating point result.
//
// Inputs:   speed and distance as integer numbers
// Outputs:  time traveled as a floating point number

// Project:      assign-2-2-2013
// File:         main.m
// Author:       Alice Wunderland
// Date:         Sep 14, 2013
// Course:       IMG204
//
// Problem Statement:
// Read an integer value representing a number of seconds, then print the
// equivalent amount of time as a combination of hours, minutes, and seconds.
// (For example, 9999 seconds is equivalent to 2 hours, 46 minutes,
// and 39 seconds.) You are expected to use only integer numbers to solve
// this problem.
//
// Inputs:   total seconds as an integer number
// Outputs:  number of hour, minutes, and seconds

int main(int argc, const char *argv[]) {
	@autoreleasepool {
		ConsoleLine *console = [[ConsoleLine alloc] init];

		//First Problem
		printf("Enter with the speed:");
		NSString *sSpeed = [console getConsoleLine];

		int iSpeed = [sSpeed intValue];

		printf("Enter with the distance:");
		NSString *sDistance = [console getConsoleLine];

		int iDistance = [sDistance intValue];
		float fTime = iDistance / [[NSNumber numberWithInt:iSpeed] floatValue];

		printf("Time = %.2f\n", fTime);

		//Second Problem
		printf("Enter with the second:");
		NSString *sSecond = [console getConsoleLine];

		//Casting String(sSecond) to Int(iSecond)
		int iSecond = [sSecond intValue];

		int iHour = iSecond / 3600;
		int iMinute = (iSecond % 3600) / 60;
		iSecond = (iSecond % 3600) % 60;

		printf("%i hour(s), %i minute(s), and %i second(s)\n", iHour, iMinute, iSecond);
	}
	return 0;
}
